var builder = WebApplication.CreateBuilder(args);

// add middlewares
builder.Services.AddControllersWithViews();
builder.Services.AddTransient<IClienteRepository, ClienteRepository>();
builder.Services.AddTransient<IFornecedorRepository, FornecedorRepository>();
builder.Services.AddTransient<IProdutoRepository, ProdutoRepository>();

var app = builder.Build();

// setup middleware
app.MapControllerRoute("default", "/{controller=Main}/{action=Index}/{id?}");
// app.MapControllers(); // using [Route("")]

app.Run();
