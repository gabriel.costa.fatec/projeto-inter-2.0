public interface IFornecedorRepository
{
    List<Fornecedor> Read();
    void Create(Fornecedor fornecedor);
    Fornecedor Read(int id);
    void Update(int id, Fornecedor fornecedor);
    void Delete(int id);

    List<Fornecedor> Search(string pesquisa);
}