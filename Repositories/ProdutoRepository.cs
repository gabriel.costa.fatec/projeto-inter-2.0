using Microsoft.Data.SqlClient;

public class ProdutoRepository : Database, IProdutoRepository
{
    public List<Fornecedor> ReadFornecedor()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Fornecedores";
        
        SqlDataReader reader = cmd.ExecuteReader();

        List<Fornecedor> fornecedores = new List<Fornecedor>();

        while(reader.Read())
        {
            Fornecedor f = new Fornecedor();
            f.IdFornecedor = reader.GetInt32(0);
            f.CNPJ = reader.GetString(1);
            f.Nome = reader.GetString(2);
            f.Email = reader.GetString(3);
            f.Telefone = reader.GetString(4);

            fornecedores.Add(f);
        }

        return fornecedores;
    }

    public void Create(Produto produto)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "INSERT INTO Produtos VALUES (@fornecedorId, @categoria, @descricao, @UM, @preco,  @qtdEstoque)";
        
        cmd.Parameters.AddWithValue("@fornecedorId", produto.FornecedorId);
        cmd.Parameters.AddWithValue("@categoria", produto.Categoria);
        cmd.Parameters.AddWithValue("@descricao", produto.Descricao);
        cmd.Parameters.AddWithValue("@UM", produto.UM);
        cmd.Parameters.AddWithValue("@preco", produto.Preco);
        cmd.Parameters.AddWithValue("@qtdEstoque", produto.QtdEstoque);

        cmd.ExecuteNonQuery();
    }

    public void Delete(int sku)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "DELETE FROM Produtos WHERE SKU = @sku";

        cmd.Parameters.AddWithValue("@sku", sku);

        cmd.ExecuteNonQuery();
    }

    public List<Produto> Read()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Produtos";
        
        SqlDataReader reader = cmd.ExecuteReader();

        List<Produto> produtos = new List<Produto>();

        while(reader.Read())
        {
            Produto p = new Produto();
            p.SKU = reader.GetInt32(0);
            p.FornecedorId = reader.GetInt32(1);
            p.Categoria = reader.GetString(2);
            p.Descricao = reader.GetString(3);
            p.UM = reader.GetString(4);
            p.Preco = reader.GetDecimal(5);
            p.QtdEstoque = reader.GetDecimal(6);

            produtos.Add(p);
        }

        return produtos;
    }

    public Produto Read(int sku)
    {
        SqlCommand cmd =  new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Produtos WHERE SKU = @sku";

        cmd.Parameters.AddWithValue("@sku", sku);

        SqlDataReader reader = cmd.ExecuteReader();

        if(reader.Read()) 
        {
            Produto p = new Produto();
            p.SKU = reader.GetInt32(0);
            p.FornecedorId = reader.GetInt32(1);
            p.Categoria = reader.GetString(2);
            p.Descricao = reader.GetString(3);
            p.UM = reader.GetString(4);
            p.Preco = reader.GetDecimal(5);
            p.QtdEstoque = reader.GetDecimal(6);

            return p;
        }

        return null;
    }

    public List<Produto> Search(string pesquisa)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "SELECT * FROM Produtos WHERE descricao like @pesquisa";
        cmd.Parameters.AddWithValue("@pesquisa", "%"+pesquisa+"%");
        
        SqlDataReader reader = cmd.ExecuteReader();

        List<Produto> produtos = new List<Produto>();

        while(reader.Read())
        {
            Produto p = new Produto();
            p.SKU = reader.GetInt32(0);
            p.FornecedorId = reader.GetInt32(1);
            p.Categoria = reader.GetString(2);
            p.Descricao = reader.GetString(3);
            p.UM = reader.GetString(4);
            p.Preco = reader.GetDecimal(5);
            p.QtdEstoque = reader.GetDecimal(6);

            produtos.Add(p);
        }

        return produtos;
    }

    public void Update(int sku, Produto produto)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        cmd.CommandText = "UPDATE Produtos SET fornecedorId = @fornecedorId, categoria = @categoria, descricao = @descricao, UM = @UM, preco = @preco,   qtdEstoque = @qtdEstoque WHERE SKU = @sku";
        cmd.Parameters.AddWithValue("@categoria", produto.Categoria);
        cmd.Parameters.AddWithValue("@descricao", produto.Descricao);
        cmd.Parameters.AddWithValue("@preco", produto.Preco);
        cmd.Parameters.AddWithValue("@UM", produto.UM);
        cmd.Parameters.AddWithValue("@fornecedorId", produto.FornecedorId);
        cmd.Parameters.AddWithValue("@qtdEstoque", produto.QtdEstoque);
        cmd.Parameters.AddWithValue("@sku", sku);

        cmd.ExecuteNonQuery();
    }
}